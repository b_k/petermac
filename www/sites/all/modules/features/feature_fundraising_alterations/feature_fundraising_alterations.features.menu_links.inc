<?php
/**
 * @file
 * feature_fundraising_alterations.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_fundraising_alterations_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_basic-page-6535:node/add/basic-page-65-35.
  $menu_links['navigation_basic-page-6535:node/add/basic-page-65-35'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/basic-page-65-35',
    'router_path' => 'node/add/basic-page-65-35',
    'link_title' => 'Basic Page (65/35)',
    'options' => array(
      'attributes' => array(
        'title' => 'Provides a basic page with a 65/35 content split.',
      ),
      'alter' => TRUE,
      'identifier' => 'navigation_basic-page-6535:node/add/basic-page-65-35',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_faq:node/add/faq.
  $menu_links['navigation_faq:node/add/faq'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/faq',
    'router_path' => 'node/add/faq',
    'link_title' => 'FAQ',
    'options' => array(
      'attributes' => array(
        'title' => 'A frequently asked question and its answer.',
      ),
      'alter' => TRUE,
      'identifier' => 'navigation_faq:node/add/faq',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -41,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_fundraiser-mini-intros:node/add/fundraiser-mini-intros.
  $menu_links['navigation_fundraiser-mini-intros:node/add/fundraiser-mini-intros'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/fundraiser-mini-intros',
    'router_path' => 'node/add/fundraiser-mini-intros',
    'link_title' => 'Fundraiser Mini Intros',
    'options' => array(
      'attributes' => array(
        'title' => 'Used to display icon+text, icon+title+text or icon+resource+text',
      ),
      'alter' => TRUE,
      'identifier' => 'navigation_fundraiser-mini-intros:node/add/fundraiser-mini-intros',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_node-export-import:node/add/node_export.
  $menu_links['navigation_node-export-import:node/add/node_export'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/node_export',
    'router_path' => 'node/add/node_export',
    'link_title' => 'Node export: import',
    'options' => array(
      'attributes' => array(
        'title' => 'Import content using <em>Node export</em>.',
      ),
      'alter' => TRUE,
      'identifier' => 'navigation_node-export-import:node/add/node_export',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -33,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_supporter-event-promo-panel:node/add/supporter-event-promo-panel.
  $menu_links['navigation_supporter-event-promo-panel:node/add/supporter-event-promo-panel'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/supporter-event-promo-panel',
    'router_path' => 'node/add/supporter-event-promo-panel',
    'link_title' => 'Supporter event promo panel',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'navigation_supporter-event-promo-panel:node/add/supporter-event-promo-panel',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -36,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_testimonial:node/add/testimonial.
  $menu_links['navigation_testimonial:node/add/testimonial'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/testimonial',
    'router_path' => 'node/add/testimonial',
    'link_title' => 'Testimonial',
    'options' => array(
      'attributes' => array(
        'title' => 'Use for Fundraising Heroes and any other testimonials',
      ),
      'alter' => TRUE,
      'identifier' => 'navigation_testimonial:node/add/testimonial',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -35,
    'customized' => 1,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Basic Page (65/35)');
  t('FAQ');
  t('Fundraiser Mini Intros');
  t('Node export: import');
  t('Supporter event promo panel');
  t('Testimonial');

  return $menu_links;
}
