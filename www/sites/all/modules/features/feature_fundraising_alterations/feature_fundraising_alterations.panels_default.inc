<?php
/**
 * @file
 * feature_fundraising_alterations.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function feature_fundraising_alterations_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'fundraising_content';
  $mini->category = '';
  $mini->admin_title = 'Fundraising content';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '89c9854b-2c2f-4d58-ad60-c7e9bca218c3';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['fundraising_content'] = $mini;

  return $export;
}
