<?php
/**
 * @file
 * feature_fundraising_alterations.features.uuid_nodequeue_item.inc
 */

/**
 * Implements hook_uuid_features_default_nodequeue_items().
 */
function feature_fundraising_alterations_uuid_features_default_nodequeue_items() {
  $nodequeue_items = array();

  $nodequeue_items[] = array(
    'queue_name' => 'after_fundraising',
    'node_uuid' => '033eda94-5a6f-4fd2-88cb-9c0c4327bfc6',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'after_fundraising',
    'node_uuid' => '39c6fd11-0ada-4580-a254-35e9fd0089e7',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'after_fundraising',
    'node_uuid' => '438db477-db61-4872-a72a-1c3c238b7053',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'after_fundraising',
    'node_uuid' => 'b05a0487-039f-486f-babd-bdbb5d836ae1',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'before_you_fundraise',
    'node_uuid' => '2a124cec-9d25-42e6-b11c-b32548ae64d9',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'before_you_fundraise',
    'node_uuid' => '3f86bc5b-5cae-43a4-8b43-36c507ae3c66',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'before_you_fundraise',
    'node_uuid' => '6222dca7-846a-4e5e-bae7-16fbd7b84937',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'before_you_fundraise',
    'node_uuid' => '936bfab0-2071-45b5-8c99-8b34e0da6660',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '112fc342-b17d-42a7-b272-5fc6833498fa',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '1b3a2840-0439-465c-9571-e89e8c935894',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '3a06d0d1-2544-43be-b49e-1f677fbd9658',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '4627a562-7e4b-4d6b-8601-e03eb6449e9e',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '5c9bcfaf-e391-4ac1-8370-50337b823098',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '657a090b-7520-4a4b-909a-7c5c2505841c',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '7f4b8e2b-c845-4393-8e2f-a095608098b1',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_ideas',
    'node_uuid' => '902ebf69-e441-415f-83c1-488e6dba4c40',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => '0fc757cf-f0a7-47cf-a47e-bc28fc9b7126',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => '50341258-0db0-4e63-b23c-713f37e7db0c',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => '730d3c76-8000-45a9-9fc3-430d5acb6977',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => '9174ac82-f7a6-4ec1-a77d-ce4691f58741',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => 'b1b8e1f3-46b0-48e1-8e22-9629cc654b4c',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => 'd8747d23-5c71-4670-8b08-bb8d7d41cb14',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => 'ed97d42e-31a0-4ef0-b8fb-0c82e19c1bb8',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'fundraising_resources',
    'node_uuid' => 'fe8d428e-edbc-4d94-9491-ae40270aee1f',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'starting_your_findraising',
    'node_uuid' => '3986b76f-2e8a-4632-8e76-32c17b77734d',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'starting_your_findraising',
    'node_uuid' => '8b49fd8f-98cd-4727-ac97-55da0a29c2cd',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'starting_your_findraising',
    'node_uuid' => 'e04054ec-3509-4db0-a569-7824187c0a9a',
  );
  $nodequeue_items[] = array(
    'queue_name' => 'starting_your_findraising',
    'node_uuid' => 'efd242b9-9e6a-4b7d-a01c-6f99fabe084e',
  );
  return $nodequeue_items;
}
