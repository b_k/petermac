<?php
/**
 * @file
 * feature_fundraising_alterations.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_fundraising_alterations_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header|node|basic_page_65_35|form';
  $field_group->group_name = 'group_header';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'basic_page_65_35';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header',
    'weight' => '1',
    'children' => array(
      0 => 'field_mainimage',
      1 => 'field_intro_copy',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-header field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_header|node|basic_page_65_35|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Header');

  return $field_groups;
}
