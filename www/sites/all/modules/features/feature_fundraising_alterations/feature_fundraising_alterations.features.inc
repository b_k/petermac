<?php
/**
 * @file
 * feature_fundraising_alterations.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_fundraising_alterations_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feature_fundraising_alterations_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function feature_fundraising_alterations_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: after_fundraising
  $nodequeues['after_fundraising'] = array(
    'name' => 'after_fundraising',
    'title' => 'After fundraising',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'fundraiser_mini_intros',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: before_you_fundraise
  $nodequeues['before_you_fundraise'] = array(
    'name' => 'before_you_fundraise',
    'title' => 'Before you fundraise',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'fundraiser_mini_intros',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: fundraising_ideas
  $nodequeues['fundraising_ideas'] = array(
    'name' => 'fundraising_ideas',
    'title' => 'Fundraising ideas',
    'subqueue_title' => '',
    'size' => 8,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'fundraiser_mini_intros',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: fundraising_resources
  $nodequeues['fundraising_resources'] = array(
    'name' => 'fundraising_resources',
    'title' => 'Fundraising resources',
    'subqueue_title' => '',
    'size' => 8,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'resources',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: starting_your_findraising
  $nodequeues['starting_your_findraising'] = array(
    'name' => 'starting_your_findraising',
    'title' => 'Starting your findraising',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'fundraiser_mini_intros',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_image_default_styles().
 */
function feature_fundraising_alterations_image_default_styles() {
  $styles = array();

  // Exported image style: promoted_square_thumb.
  $styles['promoted_square_thumb'] = array(
    'label' => 'Promoted square thumb',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 325,
          'height' => 325,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function feature_fundraising_alterations_node_info() {
  $items = array(
    'basic_page_65_35' => array(
      'name' => t('Basic Page (65/35)'),
      'base' => 'node_content',
      'description' => t('Provides a basic page with a 65/35 content split.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'fundraiser_mini_intros' => array(
      'name' => t('Fundraiser Mini Intros'),
      'base' => 'node_content',
      'description' => t('Used to display icon+text, icon+title+text or icon+resource+text'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Top level pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'secondary_page' => array(
      'name' => t('Basic Page (Secondary)'),
      'base' => 'node_content',
      'description' => t('Second level pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'supporter_event_promo_panel' => array(
      'name' => t('Supporter event promo panel'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'tertiary_page' => array(
      'name' => t('Basic Page (Tertiary)'),
      'base' => 'node_content',
      'description' => t('Third level pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'testimonial' => array(
      'name' => t('Testimonial'),
      'base' => 'node_content',
      'description' => t('Use for Fundraising Heroes and any other testimonials'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
