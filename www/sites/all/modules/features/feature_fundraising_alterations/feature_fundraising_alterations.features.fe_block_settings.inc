<?php
/**
 * @file
 * feature_fundraising_alterations.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_fundraising_alterations_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-cta_contact'] = array(
    'cache' => -1,
    'css_class' => 'cta cta-contact',
    'custom' => 0,
    'machine_name' => 'cta_contact',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Contact us',
    'visibility' => 1,
  );

  $export['block-cta_ideas'] = array(
    'cache' => -1,
    'css_class' => 'cta cta-ideas',
    'custom' => 0,
    'machine_name' => 'cta_ideas',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Stuck for an idea?',
    'visibility' => 1,
  );

  $export['block-cta_payin'] = array(
    'cache' => -1,
    'css_class' => 'cta cta-money',
    'custom' => 0,
    'machine_name' => 'cta_payin',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Pay in your fundraising money',
    'visibility' => 1,
  );

  $export['block-cta_question'] = array(
    'cache' => -1,
    'css_class' => 'cta cta-question',
    'custom' => 0,
    'machine_name' => 'cta_question',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Have a question?',
    'visibility' => 1,
  );

  $export['block-cta_resources'] = array(
    'cache' => -1,
    'css_class' => 'cta cta-tools',
    'custom' => 0,
    'machine_name' => 'cta_resources',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Useful tools',
    'visibility' => 1,
  );

  $export['block-fundraising_checklist'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'fundraising_checklist',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'petermac',
        'weight' => -23,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-fundraising-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'fundraising-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-fundraising-block_2'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'fundraising-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'petermac',
        'weight' => -24,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-fundraising-block_3'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'fundraising-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'petermac',
        'weight' => -25,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-fundraising-block_4'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'fundraising-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'petermac',
        'weight' => -22,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-promoted_content-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'promoted_content-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'petermac' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'petermac',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
