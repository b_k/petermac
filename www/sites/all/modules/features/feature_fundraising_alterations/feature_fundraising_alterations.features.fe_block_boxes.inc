<?php
/**
 * @file
 * feature_fundraising_alterations.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function feature_fundraising_alterations_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'CTA - Contact us';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cta_contact';
  $fe_block_boxes->body = '<p>We&#39;re always happy to help. If you have any further questions or concerns, call us on (03) 9656 2700, or email <a href="mailto:events@petermac.org">events@petermac.org</a></p>';

  $export['cta_contact'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'CTA - Fundraising ideas';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cta_ideas';
  $fe_block_boxes->body = '<p>Have a look through our starter list of fundraising ideas in case anything there inspires you.</p><p class="common-more"><a href="fundraising-ideas-and-tools-v2#ideas">View our fundraising ideas</a></p>';

  $export['cta_ideas'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'CTA - Pay in your fundraising money';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cta_payin';
  $fe_block_boxes->body = '<p>Have you raised some money for Peter Mac at a recent event? Thank you, you&#39;re brilliant!</p><p class="common-more"><a href="/faqs#n386">Find out how to pay it in here</a></p>';

  $export['cta_payin'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'CTA - Have a question?';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cta_question';
  $fe_block_boxes->body = '<p>We&#39;ve pulled the key questions we get from fundraisers into a useful list, so you might find your answer here.</p><p class="common-more"><a href="/fundraising-faqs">Check out our fundraising FAQs</a></p>';

  $export['cta_question'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'CTA - Fundraising resources';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cta_resources';
  $fe_block_boxes->body = '<p>We&#39;ve got a range of tools for you to download, including our helpful Fundraising Guidelines booklet.</p><p class="common-more"><a href="fundraising-ideas-and-tools-v2#tools">Fundraising tools to download</a></p>';

  $export['cta_resources'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Fundraising checklist introduction';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'fundraising_checklist';
  $fe_block_boxes->body = '<div class="breaker-20">&nbsp;</div><div class="grid-container"><div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent"><div class="grid-100 tablet-grid-100 mobile-grid-100"><div class="in-grid-item-wrapper common-inner pad-20-wrapper clearfix"><div class="restricted-width-75"><h2>Fundraising checklist</h2><p>Starting your own fundraising activity is actually straightforward, but we know it can be daunting when you&rsquo;re just starting out. Our fundraising step-by-step guide should point you in the right direction. It&rsquo;s also a great way to make sure nothing gets forgotten.</p></div></div></div></div></div>';

  $export['fundraising_checklist'] = $fe_block_boxes;

  return $export;
}
