/* $Id: donate.js,v 1.2 2010/09/30 21:35:55 jerdavis Exp $ */
/**
 * @file
 * jQuery functions for the donation form
 */
(function ($) {
Drupal.behaviors.petermac_donate_simple = {
  attach: function (context) {
    var that = this;
    this.display;

    this.init = function(){
        if(!$('#donation-pageform').length) return;

        this.display = $('#donation-pageform');
        this.addListeners();
        this.changed();
    }

    this.addListeners = function(){
        $('input[type=radio]', this.display).change(function(){
            that.changed();
        });
        $('input.form-text', this.display).focus(function(){
            that.inputfocus(this);
        });
    }

    this.changed = function(){
        var trg;

        $('input[type=radio]', this.display).each(function(i){
            if($(this).prop('checked')) trg = this;
        });

        $('.selected' , this.display).removeClass('selected');

        if(trg != undefined) $(trg).parent().addClass('selected');
    }

    this.inputfocus = function(trg){
        $('input[type=radio]', $(trg).parent().parent()).prop('checked', true);
        that.changed();
    }

    this.init();

  }
}
})(jQuery);
