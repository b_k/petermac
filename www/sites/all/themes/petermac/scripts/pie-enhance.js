$(function() {
    if (window.PIE) {
        $('.green-button').each(function() {
            PIE.attach(this);
        });
        $('.pink-button').each(function() {
            PIE.attach(this);
        });  
        $('.red-button').each(function() {
            PIE.attach(this);
        });               
        $('.blue-button').each(function() {
            PIE.attach(this);
        });    
        $('.button-wrapper-green').each(function() {
            PIE.attach(this);
        });        
        $('.button-wrapper-pink').each(function() {
            PIE.attach(this);
        });        
        $('.button-wrapper-red').each(function() {
            PIE.attach(this);
        });        
        $('.button-wrapper-blue').each(function() {
            PIE.attach(this);
        });        
    }
});