<?php

function  petermac_preprocess(&$vars, $hook) {
  if (isset($vars['classes_array'])) {
    $vars['attributes_array']['class'] = $vars['classes_array'];
  }

  // Run all inc files
  petermac_invoke('preprocess', $hook, $vars);
}


function petermac_fieldset($variables){
  $element = $variables['element'];

  if (isset($element['#id']) && $element['#id'] == 'edit-cart-contents') {
    $element['#title'] = '';
  }

  element_set_attributes($element, array('id'));

  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}

/**
 * Implements hook_process().
 */
function petermac_process(&$vars, $hook) {
  if (!empty($vars['elements']['#grid']) || !empty($vars['elements']['#data']['wrapper_css'])) {
    if (!empty($vars['elements']['#grid'])) {
      foreach (array('prefix', 'suffix', 'push', 'pull') as $quality) {
        if (!empty($vars['elements']['#grid'][$quality])) {
          array_unshift($vars['attributes_array']['class'], $quality . '-' . $vars['elements']['#grid'][$quality]);
        }
      }
      array_unshift($vars['attributes_array']['class'], 'grid-' . $vars['elements']['#grid']['columns']);
    }
    if (!empty($vars['elements']['#data']['wrapper_css'])) {
      foreach (array_map('drupal_html_class', explode(' ', $vars['elements']['#data']['wrapper_css'])) as $class) {
        $vars['attributes_array']['class'][] = $class;
      }
    }
    $vars['attributes'] = $vars['attributes_array'] ? drupal_attributes($vars['attributes_array']) : '';
  }
  if (!empty($vars['elements']['#grid_container']) || !empty($vars['elements']['#data']['css'])) {
    if (!empty($vars['elements']['#data']['css'])) {
      foreach (array_map('drupal_html_class', explode(' ', $vars['elements']['#data']['css'])) as $class) {
        $vars['content_attributes_array']['class'][] = $class;
      }
    }
    if (!empty($vars['elements']['#grid_container'])) {
      $vars['content_attributes_array']['class'][] = 'container-' . $vars['elements']['#grid_container'];
    }
    $vars['content_attributes'] = $vars['content_attributes_array'] ? drupal_attributes($vars['content_attributes_array']) : '';
  }
}


/**
 * Helper function to load inc files.
 */
function petermac_invoke($type, $hook, &$variables) {
  global $theme_key;

  // The name of the function to look for (e.g. mytheme_process_node).
  $function = $theme_key . '_' . $type . '_' . $hook;

  // If the function doesn't exist within template.php, look for the
  // appropriate include file.
  if (!function_exists($function)) {
    // The file to search for (e.g. process/node.inc).
    $file = drupal_get_path('theme', $theme_key) . '/' . $type . '/' . $type . '-' . str_replace('_', '-', $hook) . '.inc';

    // If the file exists, include it.
    if (is_file($file)) {
      include($file);
    }
  }

  // Try to call the function again.
  if (function_exists($function)) {
    $function($variables);
  }
}

/**
 * Override theme_breadrumb().
 *
 * Print breadcrumbs as a list, with separators.
 */
function petermac_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  //print_R($breadcrumb);
  if (!empty($breadcrumb)) {
    $breadcrumbs = '';

    $count = count($breadcrumb) - 1;
    foreach ($breadcrumb as $key => $value) {
      if ($count != $key) {
        $breadcrumbs .= $value . ' &gt; ';
      }
      else{
        $breadcrumbs .=  $value ;
      }
    }

    $node = menu_get_object();

    list($crappymenu, $donatesection) = petermac_check_crappy(isset($node->type) ? $node : null);

    if ($crappymenu){
      if ($donatesection == 1) $breadcrumbs .= ' &gt; <span class="active">Donation details</span>';
      if ($donatesection == 2) $breadcrumbs .= ' &gt; <span class="active">Personal details</span>';
      if ($donatesection == 3) $breadcrumbs .= ' &gt; <span class="active">Review details</span>';
      if ($donatesection == 4) $breadcrumbs .= ' &gt; <span class="active">Payment details</span>';
    }
    else{
      $breadcrumbs .= ' &gt; <span class="active">'. drupal_get_title() .'</span>';
    }
    return $breadcrumbs;
  }
}


function petermac_addthis_element($variables) {
  $element = $variables['addthis_element'];
  $element['#attributes']['class'] = array();
  $element['#attributes']['class'][] = $element['#addthis_service'];
  $output = $output = '<' . $element['#tag'] . drupal_attributes($element['#attributes']) . '>';
  $output .= $element['#value'];
  $output .= '</' . $element['#tag'] . ">\n";
  return $output;
}


function petermac_addthis_wrapper($variables) {
  $output = '<div class="grid-33 tablet-grid-33 mobile-grid-50 mobile-push-50 right-align mobile-breaker-10" id="header-aux-social">';
  $output .= '<div class="in-grid-item-wrapper">';
  $element = $variables['addthis_wrapper'];
  $children = element_children($element);

  if (count($children) > 0) {
    foreach ($children as $child) {
      $output .= render($element[$child]);
    }
  }
  $output .= "</div>\n</div>";
  return $output;
}


function petermac_js_alter(&$javascript) {
  // Update jquery version for non-administration pages
  if (arg(0) != 'admin' && arg(0) != 'panels' && arg(0) != 'ctools'  && !(module_exists('jquery_update'))) {

    $jquery_file = drupal_get_path('theme', 'petermac') . '/scripts/jquery-1.10.2.min.js';
    $jquery_version = '1.10.2';

    $migrate_file = drupal_get_path('theme', 'petermac') . '/scripts/jquery-migrate-1.2.1.min.js';
    $migrate_version = '1.2.1';

    $form_file = drupal_get_path('theme', 'petermac') . '/scripts/jquery-form-3.31.0.min.js';
    $form_version = '3.31.0';

    $javascript['misc/jquery.js']['data'] = $jquery_file;
    $javascript['misc/jquery.js']['version'] = $jquery_version;
    $javascript['misc/jquery.js']['weight'] = 0;
    $javascript['misc/jquery.js']['group'] = -101;

    drupal_add_js($migrate_file);

    if (isset($javascript["$migrate_file"])) {
      $javascript["$migrate_file"]['version'] = $migrate_version;
      $javascript["$migrate_file"]['weight'] = 1;
      $javascript["$migrate_file"]['group'] = -101;
    }
    if (isset($javascript['misc/jquery.form.js'])) {
      $javascript['misc/jquery.form.js']['data'] = $form_file;
      $javascript['misc/jquery.form.js']['version'] = $form_version;
      $javascript['misc/jquery.form.js']['weight'] = 2;
      $javascript['misc/jquery.form.js']['group'] = -101;
    }
  }

  drupal_add_js(drupal_get_path('theme', 'petermac') . '/scripts/panels.js');

  $exclude = array(
    drupal_get_path('module', 'panels') . '/js/panels.js' => FALSE,
  );

  if (arg(0) != 'admin' || !user_access('administer site configuration')) {
    $javascript = array_diff_key($javascript, $exclude);
  }
}



function petermac_css_alter(&$css)
{

  // Remove some Drupal base themes.
  $exclude = array(
    drupal_get_path('module', 'oembed') . '/oembed.theme.css' => FALSE,
    drupal_get_path('module', 'ctools') . 'css/ctools.css' => FALSE,
    drupal_get_path('module', 'oembed') . '/oembed.base.css' => FALSE,
    'misc/vertical-tabs.css' => FALSE,
    drupal_get_path('module', 'aggregator') . '/aggregator.css' => FALSE,
    drupal_get_path('module', 'block') . '/block.css' => FALSE,
    drupal_get_path('module', 'book') . '/book.css' => FALSE,
    drupal_get_path('module', 'comment') . '/comment.css' => FALSE,
    drupal_get_path('module', 'dblog') . '/dblog.css' => FALSE,
    drupal_get_path('module', 'field') . '/theme/field.css' => FALSE,
    drupal_get_path('module', 'file') . '/file.css' => FALSE,
    drupal_get_path('module', 'filter') . '/filter.css' => FALSE,
    drupal_get_path('module', 'forum') . '/forum.css' => FALSE,
    drupal_get_path('module', 'help') . '/help.css' => FALSE,
    drupal_get_path('module', 'menu') . '/menu.css' => FALSE,
    drupal_get_path('module', 'node') . '/node.css' => FALSE,
    drupal_get_path('module', 'node') . '/openid.css' => FALSE,
    drupal_get_path('module', 'profile') . '/profile.css' => FALSE,
    drupal_get_path('module', 'search') . '/search.css' => FALSE,
    drupal_get_path('module', 'statistics') . '/statistics.css' => FALSE,
    drupal_get_path('module', 'syslog') . '/syslog.css' => FALSE,
    drupal_get_path('module', 'system') . '/admin.css' => FALSE,
    drupal_get_path('module', 'system') . '/maintenance.css' => FALSE,
    drupal_get_path('module', 'poll') . '/poll.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.admin.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.base.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.maintenance.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.menus.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.messages.css' => FALSE,
    drupal_get_path('module', 'system') . '/system.theme.css' => FALSE,
    drupal_get_path('module', 'taxonomy') . '/taxonomy.css' => FALSE,
    drupal_get_path('module', 'tracker') . '/tracker.css' => FALSE,
    drupal_get_path('module', 'update') . '/update.css' => FALSE,
    drupal_get_path('module', 'user') . '/user.css' => FALSE,
    drupal_get_path('module', 'views') . '/css/views.css' => FALSE,
    drupal_get_path('module', 'panels') . '/css/panels.css' => FALSE,
    drupal_get_path('module', 'views_slideshow') . '/views_slideshow.css' => FALSE
  );

  if (arg(0) != 'admin' || ! user_access('administer site configuration')) {
    $css = array_diff_key($css, $exclude);
  }
}


/**
 * Implementation of hook_preprocess_HOOK().
 */
function petermac_preprocess_webform_form(&$variables) {
  //dpm($variables);
}

function petermac_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'search_block_form') {

    $form['search_block_form']['#title_display'] = 'invisible';
    $form['search_block_form']['#attributes']['class'][] = 'input-medium search-query search-input';
    $form['search_block_form']['#attributes']['placeholder'] = t('Search...');
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search...';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search...') {this.value = '';}";
    $form['actions']['submit']['#attributes']['class'][] = 'btn-search search-submit no-show-text';
  }
  if ($form_id == 'user_login_block'){
    unset($form['links']);
    $items = array();
    $items[] = l(t('Forgot your password ?'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.'))));
    $items[] = l(t('Not a member? Register now.'), 'user/register', array('attributes' => array('title' => t('Register now.'))));
    $form['links'] = array('#markup' =>  '<p>'.l(t('Forgot your password ?'), 'user/password', array('attributes' => array('title' => t('Request new password via email.')))).'<br />'.l(t('Not a member? Register now.'), 'user/register', array('attributes' => array('title' => t('Register now.')))).'</p>');
  }
  if ($form_id == 'user_register_form'){
    $form['account']['name']['#description'] = t('For username you can use spaces; punctuation is not accepted except for periods, hyphens, apostrophes, and underscores. ');
  }
  if ($form_id == 'user_profile_form'){
    $form['account']['mail']['#title']= t('Email address');
    $form['account']['mail']['#description'] = t('A valid email address. All emails from the system will be sent to this address. The email address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by email.');
  }
  if ($form['#node']->vuuid == 'b3db6acb-0891-4137-9934-3c644a3b3a4e'){

    //dpm($form);

    // Add a class to the form that makes sense.
    $form['#attributes']['class'][] = 'fundraising-application-form';

    // Get the value of step number and the total number of steps in form.
    $page_num = $form['details']['page_num']['#value'];
    $page_count = $form['details']['page_count']['#value'];

    // Check if its not the last step and catcha present in form.
    if (($page_num != $page_count) && (!empty($form['captcha']))) {
      // Remove the captacha from the form.
      unset($form['captcha']);
    }

    if ($page_num > 1) {
      drupal_add_js("/sites/all/themes/petermac/scripts/formscroll.js");
    }

    $types = array('text', 'textfield', 'textarea', 'select_or_other_select', 'email');
    foreach ($form['submitted'] as $key => $value) {
      if(isset($value['#webform_component']['type']) && in_array($value['#webform_component']['type'], $types)){
        $form['submitted'][$key]['#attributes']['class'][] = 'form-control';
      }
    }
  }
}

/*
 * Implements hook_menu_link
 * Apply bootstrap menu classes to all menu blocks in the
 * navigation region and the main-menu block by default.
 * Note: if a menu is in the navigation and somewhere else as well,
 *       both instances of the menu will have the classes applied,
 *       not just the one in the navigation
 */

function petermac_menu_grid($block_count) {
  $available_width = 100;
  if ($block_count != 0) {
    $percent = $available_width / $block_count;
    $span = floor($percent);
    return $span;
  }
  return 100;
}


function petermac_menu_tree($variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}


function petermac_menu_link(array $vars) {

  $element = $vars['element'];
  if (petermac_is_in_nav_menu($element)) {
    $sub_menu = '';
    $below = 0;
    //echo $element['#original_link']['depth'];
    if ($element['#below']) {
      // Add our own wrapper
      unset($element['#below']['#theme_wrappers']);
      if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {

        if ((count($element['#below']) - 1) == 1){
        }
        else{
          $sub_menu = '<ul' . drupal_attributes($element['#attributes']) . '>'. drupal_render($element['#below']) . '</ul>' ;
        }
      }
      elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
        $num_top_level_children = count(menu_tree_page_data('main-menu', 0));
        $span = petermac_menu_grid($num_top_level_children);
        //$element['#attributes']['class'][] = 'grid-'.$span;
        $sub_menu = '<div class="nav-sub"><ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul></div>';
      }

      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

      // Check if this element is nested within another
      if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
        $element['#attributes']['class'][] = 'dropdown-submenu';
      }
      else {
        $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['html'] = TRUE;
        //$element['#title'] .= ' <span class="caret"></span>';
      }
      // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
      $element['#localized_options']['attributes']['data-target'] = '#';
    }
    else{
      $span = 100;
      if ($element['#original_link']['depth'] == 1){
        $num_top_level_children = count(menu_tree_page_data('main-menu', 0));
        $span = petermac_menu_grid($num_top_level_children);
        //$element['#attributes']['class'][] = 'grid-'.$span;
      }
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

  } else {
    $element = $vars['element'];
    $sub_menu = '';

    if ($element['#below']) {
      $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }

}


/*
 *  Find out if an element (a menu link) is a link displayed in the
 *  navigation region for the user. We return true by default if this is a
 *  menu link in the main-menu. Open Framework treats the main-menu
 *  as being in the navigation by default.
 *  We are using the theming functions to figure out the block IDs.
 *  The block IDs aren't passed to this function, but theming function names are,
 *  and those are baed on the block ID.
 *
 */

function petermac_is_in_nav_menu($element) {

  // #theme holds one or more suggestions for theming function names for the link
  // simplify things by casting into an array
  $link_theming_functions = (array)$element['#theme'];

  // Avoid calculating this more than once
  $nav_theming_functions = &drupal_static(__FUNCTION__);

  // if not done yet, calculate the names of the theming function for all the blocks
  // in the navigation region

  if (!isset($nav_theming_functions)) {

    // get all blocks in the navigation region
    $blocks = block_list('main-menu');

    // Blocks placed using the context module don't show up using Drupal's block_list
    // If context is enabled, see if it has placed any blocks in the navigation area
    // See: http://drupal.org/node/785350
    $context_blocks = array();

    if (module_exists('context')) {
      $reaction_block_plugin = context_get_plugin('reaction', 'block');
      $context_blocks = $reaction_block_plugin->block_list('main-menu');
    }

    $blocks = array_merge($blocks, $context_blocks);

    // extract just their IDs (<module>_<delta>)
    $ids = array_keys($blocks);

    // translate the ids into function names for comparison purposes
    $nav_theming_functions = array_map('petermac_block_id_to_function_name', $ids);

  }

  // if there is nothing in the navigation section, the main menu is added automatically, so
  // we watch for that.
  // 'menu_link__main_menu' is the theming function name for the main-menu
  if ((empty($nav_theming_functions)) && (in_array('menu_link__main_menu', $link_theming_functions))) {
    return TRUE;
  };

  // Find out if any of the theming functions for the blocks are the same
  // as the theming functions for the link.
  $intersect = array_intersect($nav_theming_functions, $link_theming_functions);
  if ((!empty($intersect))) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/*
 *  Convert a block id to a theming function name
 */

function petermac_block_id_to_function_name ($id) {
  // if a system block, remove 'system_'
  $id = str_replace('system_', '', $id);
  // recognize menu and block_menu module blocks
  if (strpos($id, 'menu_block_') === false) {
    // if a menu block but not a menu_block block, remove menu_
    $id = str_replace('menu_',       '', $id);
  }
  else {
    // if a menu_block block, keep menu_block, but add an
    // underscore. Not sure why this is different from other
    // core modules
    $id = str_replace('menu_block_', 'menu_block__', $id);
  }
  // massage the id to looks like a theming function name
  // use the same function used to create the name of theming function
  $id = strtr($id, '-', '_');
  $name = 'menu_link__' . $id;
  return $name;
}

function petermac_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);
}



function petermac_webform_element($variables) {
  $element = $variables['element'];
  // Ensure defaults.
  // dpm($variables, 'Variables');
  $variables['element'] += array(
    '#title_display' => 'before',
  );

  $element = $variables['element'];

  // All elements using this for display only are given the "display" type.
  if (isset($element['#format']) && $element['#format'] == 'html') {
    $type = 'display';
  }
  else {
    $type = (isset($element['#type']) && !in_array($element['#type'], array('markup', 'textfield', 'webform_email', 'webform_number'))) ? $element['#type'] : $element['#webform_component']['type'];
  }

  // Convert the parents array into a string, excluding the "submitted" wrapper.
  $nested_level = $element['#parents'][0] == 'submitted' ? 1 : 0;
  $parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));

  $wrapper_classes = array(
    'form-item',
    'webform-component',
    'webform-component-' . $type,
    'webform-component--' . $parents,
  );
  if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0) {
    $wrapper_classes[] = 'webform-container-inline';
  }
  if ($type=='radios' && count($element['#options']) == 2){
    $wrapper_classes[] = 'inline-cleared-radio';
  }
  if ($type=='select' && count($element['#options']) == 2 && $parents == 'publicity-and-marketing--obtain-appropriate-approval'){
    $wrapper_classes[] = 'inline-cleared-radio';
  }
  $output = '<div class="' . implode(' ', $wrapper_classes) . '" id="webform-component-' . $parents . '">' .  "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . _webform_filter_xss($element['#field_prefix']) . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . _webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  switch ($element['#title_display']) {
    case 'inline':
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

function petermac_select_or_other($variables) {
  //print_R($variables);

  $element = $variables['element'];

  $output = null;

  if ($element['#title_display'] != 'none') {
    $output .= '<label>'.$element['select']['#title'].'</label>';
    $element['select']['#title_display'] = 'invisible';
  }

  if (isset($variables['element']['#entity']) && $variables['element']['#entity']->type == 'commerce_donate' ){
    $children = form_process_radios($element['select']);
    $output .= "<div class=\"select-or-other\">\n";
    $output .= drupal_render_children($element) . "\n";
    $output .= "</div>\n";
  }
  else{
    $output .= "<div class=\"select-or-other\">\n";
    $output .= drupal_render_children($element) . "\n";
    $output .= "</div>\n";
  }
  return $output;
}


function petermac_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  foreach (array('rel', 'alt', 'title') as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  return '<img' . drupal_attributes($attributes) . ' />';
}


function petermac_check_crappy($node=null)
{
  $crappymenu = false;
  $donatesection = 0;
  if (isset($node->type)) {
    if ($node->type == 'webform' || $node->type == 'petermac_donate_simple') {
      if ($node->type == 'petermac_donate_simple'){
        $crappymenu = true;
        $donatesection = 1;
      }

      if (isset($node->webform)){
        foreach ($node->webform['components'] as $cid => $component){

          $donatesection = 1;

          if ($component['type'] == 'productfield') {
            $crappymenu = true;
          }

          if (strpos($component['form_key'],'donationamount') !== false) {
            $crappymenu = true;
          }
        }
      }
    }
  }


  return array(0 => $crappymenu, 1 => $donatesection);
}



function petermac_theme(&$existing, $type, $theme, $path) {
  // If we are auto-rebuilding the theme registry, warn about the feature.
  if (
    isset($GLOBALS['user']) && function_exists('user_access') && user_access('administer site configuration')
    && theme_get_setting('petermac_rebuild_registry')
    && (arg(0) == 'admin' || flood_is_allowed($GLOBALS['theme'] . '_rebuild_registry_warning', 3))
  ) {
    flood_register_event($GLOBALS['theme'] . '_rebuild_registry_warning');
    drupal_set_message(t('For easier theme development, the theme registry is being rebuilt on every page request. It is <em>extremely</em> important to <a href="!link">turn off this feature</a> on production websites.', array('!link' => url('admin/appearance/settings/' . $GLOBALS['theme']))), 'warning', FALSE);
  }

  return array(
    'petermac_links' => array(
      'variables' => array(
        'links' => array(),
        'attributes' => array(),
        'heading' => NULL
      ),
    ),
    'petermac_btn_dropdown' => array(
      'variables' => array(
        'links' => array(),
        'attributes' => array(),
        'type' => NULL
      ),
    ),
    'petermac_modal' => array(
      'variables' => array(
        'heading' => '',
        'body' => '',
        'footer' => '',
        'attributes' => array(),
        'html_heading' => FALSE,
      ),
    ),
    'petermac_accordion' => array(
      'variables' => array(
        'id' => '',
        'elements' => array(),
      ),
    ),
    'petermac_search_form_wrapper' => array(
      'render element' => 'element',
    ),
    'petermac_append_element' => array(
      'render element' => 'element',
    ),
  );
}


function _petermac_var($var_name, $new_val = NULL) {
  $vars = &drupal_static(__FUNCTION__, array());

  // If a new value has been passed
  if ($new_val) {
    $vars[$var_name] = $new_val;
  }

  return isset($vars[$var_name]) ? $vars[$var_name] : NULL;
}
