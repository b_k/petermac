<?php

function petermac_preprocess_page(&$vars)
{
    // Primary nav
  $vars['primary_nav'] = FALSE;

  if ($vars['main_menu']) {
    // Build links
    $vars['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    // Provide default theme wrapper function
    $vars['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }

  // Secondary nav
  $vars['secondary_nav'] = FALSE;
  if ($vars['secondary_menu']) {
    // Build links
    $vars['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function
    $vars['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }

  // Some template variables
  list($crappymenu, $donatesection) = petermac_check_crappy(isset($vars['node']) ? $vars['node'] : null);
  $vars['crappymenu'] = $crappymenu;
  $vars['donatesection'] = $donatesection;


  // Checks if tabs are set
  if (!isset($vars['tabs']['#primary'])) $vars['tabs']['#primary'] = FALSE;

  // Replace tabs with drop down version
  $vars['tabs']['#primary'] = _bootstrap_local_tasks($vars['tabs']['#primary']);

  if (!empty($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__node__' . $vars['node']->type;
  }

  // check if nav should be shown for landing pages
if($vars['node']->type == 'landing') {
  if ($vars['page']['content']['system_main']['nodes'][$vars['node']->nid]['field_hide_navigation']['#object']->field_hide_navigation['und'][0]['value'] == 1) {
    $vars['hide-nav_var'] = _petermac_var('hide-nav-class', 'hide-navigation');
  }
}

}





/**
* Get all primary tasks including subsets
*/
function _bootstrap_local_tasks($tabs = FALSE) {
  if ($tabs == '') {
    return $tabs;
  }

  if (!$tabs) {
    $tabs = menu_primary_local_tasks();
  }

  foreach ($tabs as $key => $element) {
    $result = db_select('menu_router', NULL, array('fetch' => PDO::FETCH_ASSOC))
      ->fields('menu_router')
      ->condition('tab_parent', $element['#link']['path'])
      ->condition('context', MENU_CONTEXT_INLINE, '<>')
      ->condition('type', array(MENU_DEFAULT_LOCAL_TASK, MENU_LOCAL_TASK), 'IN')
      ->orderBy('weight')
      ->orderBy('title')
      ->execute();

    $router_item = menu_get_item($element['#link']['href']);
    $map = $router_item['original_map'];

    $i = 0;
    foreach ($result as $item) {
      _menu_translate($item, $map, TRUE);

      //only add items that we have access to
      if ($item['tab_parent'] && $item['access']) {
        //set path to that of parent for the first item
        if ($i === 0) {
          $item['href'] = $element['#link']['href'];
        }

        if (current_path() == $item['href']) {
          $tabs[$key][] = array(
          '#theme' => 'menu_local_task',
          '#link' => $item,
          '#active' => TRUE,
          );
        }
        else {
          $tabs[$key][] = array(
          '#theme' => 'menu_local_task',
          '#link' => $item,
          );
        }

        //only count items we have access to.
        $i++;
      }
    }
  }

  return $tabs;
}
