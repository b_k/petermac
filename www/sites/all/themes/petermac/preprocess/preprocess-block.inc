<?php

function petermac_preprocess_block(&$vars) {
  // Count number of blocks in a given theme region
  $vars['block_count'] = count(block_list($vars['block']->region));

  if ($vars['block']->module == 'search') {
    $vars['attributes_array']['role'] = 'search';
  }
}