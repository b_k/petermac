<?php

function petermac_preprocess_region(&$vars)
{
    $vars['theme_hook_suggestions'][] = 'region__' . $vars['region'];
}
