<?php

function petermac_preprocess_node(&$vars)
{
    if ($vars['view_mode'] == 'full') {
        $alias = drupal_get_path_alias('node/' . $vars['node']->nid);

        if ( ! empty($alias)) {
            $alias = str_replace('/', '--', $alias);
            $vars['theme_hook_suggestions'][] = 'node__' . trim($alias);
        }

        if ($vars['node']->type == 'external_page_shortcut') {
            if ( ! empty($vars['node']->field_external_link['und'][0]['url'])) {
                header('Location: '.$vars['node']->field_external_link['und'][0]['url']);
                exit;
            } else {
                drupal_set_message('Oops,this link is no longer working.', 'error');
            }
        }

        if ($vars['node']->vuuid == 'b3db6acb-0891-4137-9934-3c644a3b3a4e') {
          $vars['theme_hook_suggestions'][] = 'node__fundraising_application';
          // kpr($vars['theme_hook_suggestions']);
        }

    }

}
