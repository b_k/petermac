<?php

/**
* @file
* Container for hook_preprocess_html().
*/

function petermac_preprocess_html(&$vars) {

  // theme option variables
  $vars['content_order_classes'] = theme_get_setting('content_order_classes');
  $vars['front_heading_classes'] = theme_get_setting('front_heading_classes');
  $vars['breadcrumb_classes'] = theme_get_setting('breadcrumb_classes');
  $vars['border_classes'] = theme_get_setting('border_classes');
  $vars['corner_classes'] = theme_get_setting('corner_classes');
  $vars['body_bg_type'] = theme_get_setting('body_bg_type');
  $vars['body_bg_classes'] = theme_get_setting('body_bg_classes');
  $vars['body_bg_path'] = theme_get_setting('body_bg_path');

  // Not sure about this if statement at the moment, Al.
  if (arg(0) != 'admin') {
    drupal_add_css(drupal_get_path('theme', 'petermac') . '/styles/screen.css', array('group' => CSS_SYSTEM, 'every_page' => TRUE, 'weight' => -1003));
    drupal_add_css(drupal_get_path('theme', 'petermac') . '/styles/meanmenu.css', array('group' => CSS_SYSTEM, 'every_page' => TRUE, 'weight' => -1002));
    drupal_add_css(drupal_get_path('theme', 'petermac') . '/styles/mods.css', array('group' => CSS_THEME, 'every_page' => TRUE, 'weight' => 1000));
  }

  $context = explode('/', drupal_get_path_alias());
  $context = reset($context);

  if (!empty($context)) {
   $vars['attributes_array']['class'][] = drupal_html_class('context-' . $context);
  }
  $vars['attributes'] = ' class="' . implode(" ", $vars['attributes_array']['class']).'"';

  if (_petermac_var('hide-nav-class')) {
    $vars['classes_array'][] = _petermac_var('hide-nav-class');
  }

}
