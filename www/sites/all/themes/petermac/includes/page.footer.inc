</div>

<!-- /#main, /#main-wrapper -->
<?php if ($page['footer']): ?>

<footer role="contentinfo">
  <div class="three-color-strip">

  </div>
  <div class="breaker-30 clearfix"></div>


  <div class="grid-container">

    <?php if ($page['footer_left'] && !$crappymenu): ?>
      <div class="grid-65 tablet-grid-65 mobile-grid-100 grid-parent">
        <?php print render($page['footer_left']);?>
      </div>
    <?php endif;?>


    <?php if ($page['footer_right'] && !$crappymenu): ?>
      <div class="grid-35 tablet-grid-35 mobile-grid-100 right-align cta-buttons mobile-left-align">
        <?php
            if (!$crappymenu) {
                $link = [
                    '#theme'   => 'link',
                    '#text'    => 'DONATE NOW',
                    '#path'    => theme_get_setting('donate_form'),
                    '#options' => ['attributes' => ['title' => 'Donate to Peter Mac Now'], 'html' => false],
                    '#prefix'  => '<div style="margin-top:50px;" class="button-wrapper-green set-width left-align"><span class="donate-icon"></span> ',
                    '#suffix'  => '</div>',
                ];

                print render($link);
            }
        ?>
  	    <div class="breaker-20 clearfix"></div>
        <?php print render($page['footer_right']);?>
	    </div>
	  <?php endif;?>

  </div>



<div class="breaker-20 clearfix"></div>

<div class="grid-container">
	<div class="grid-100 tablet-grid-100 mobile-grid-100">
		<div class="hr-style hide-on-mobile"></div>
	</div>
</div>
<?php if ($main_menu && !$crappymenu): ?>
<div class="breaker-20 clearfix"></div>
<div class="grid-container">
	<div class="grid-100 tablet-100 hide-on-mobile">
		<nav>
      <?php
          $menu = menu_navigation_links('menu-footer-sitemap');
          print theme('links__menu_footer_sitemap', ['links' => $menu]);
      ?>
    </nav>
	</div>
</div>
<?php endif;?>
<!-- Petermac Donation page-->
<?php if (drupal_get_path_alias(current_path()) == 'donate-to-petermac'): ?>
<div class="breaker-20 clearfix"></div>
<div class="grid-container donate-footer-link">
  <div class="grid-100 tablet-100">
    <nav>
       <ul>
          <li><a href="/privacy-policy" title="">Privacy Policy</a></li>
       </ul>
    </nav>
  </div>
</div>
<?php endif;?>

<?php if ($page['sub_footer']): ?>
	<div id="sub-footer">
		<div class="grid-container">
			<div class="grid-100">
				<?php print render($page['sub_footer']);?>
			</div>
		</div>
	</div>
<?php endif;?>
</footer>
<!-- /#footer -->
<?php endif;?>