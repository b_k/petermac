<?php
    if (drupal_is_front_page()) {
        $thisnode = node_load(1);
        $containerClass = 'home-four';
    } else {
        $thisnode = menu_get_object();
        $containerClass = 'interest-four';
   }
?>

<?php if ( ! empty($thisnode->field_linked_pages)): ?>

    <div class="breaker-20 clearfix"></div>

    <?php if ( ! drupal_is_front_page()): ?>
        <div class="grid-container extra-outer-gutter interest-four">
            <div class="grid-100 tablet-grid-100 mobile-grid-100 common-inner">
                <div class="in-grid-item-wrapper box-25-wrapper common-inner">
                    <h3>You may also be interested in...</h3>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="grid-container extra-outer-gutter mobile-reset-gutter <?=$containerClass?>">
        <div class="grid-100 tablet-grid-100 mobile-grid-100 common-inner grid-parent">

            <?php if (isset($thisnode->field_linked_pages['und'][0]['url'])): ?>
                <?php
                    $url = str_replace(url(NULL, array('absolute' => TRUE)), '', $thisnode->field_linked_pages['und'][0]['url']);
                    $source = drupal_lookup_path('source', $url,'');
                    $nidParts = explode("/", $source);
		    $nidPart = (isset($nidParts[1]) ? $nidParts[1] : null);
                ?>

                <div class="grid-25 tablet-grid-50 mobile-grid-100 common-inner equal">
                    <div class="in-grid-item-wrapper common-inner">
                        <?php print views_embed_view('intro_block','home_four', $nidPart); ?>
                    </div>
                </div>
            <?php endif; ?>



            <?php if (isset($thisnode->field_linked_pages['und'][1]['url'])): ?>
                <?php
                    $url = str_replace(url(NULL, array('absolute' => TRUE)), '', $thisnode->field_linked_pages['und'][1]['url']);
                    $source = drupal_lookup_path('source', $url,'');
                    $nidParts = explode("/", $source);
                    $nidPart = (isset($nidParts[1]) ? $nidParts[1] : null);
                ?>

                <div class="grid-25 tablet-grid-50 mobile-grid-100 common-inner equal">
                    <div class="in-grid-item-wrapper common-inner">
                        <?php print views_embed_view('intro_block','home_four', $nidPart); ?>
                    </div>
                </div>
            <?php endif; ?>



            <?php if (isset($thisnode->field_linked_pages['und'][2]['url'])): ?>
                <?php
                    $url = str_replace(url(NULL, array('absolute' => TRUE)), '', $thisnode->field_linked_pages['und'][2]['url']);
                    $source = drupal_lookup_path('source', $url,'');
                    $nidParts = explode("/", $source);
                    $nidPart = (isset($nidParts[1]) ? $nidParts[1] : null);
                ?>

                <div class="grid-25 tablet-grid-50 mobile-grid-100 common-inner equal">
                    <div class="in-grid-item-wrapper common-inner">
                        <?php print views_embed_view('intro_block','home_four', $nidPart); ?>
                    </div>
                </div>
            <?php endif; ?>



            <?php if (isset($thisnode->field_linked_pages['und'][3]['url'])): ?>
                <?php
                    $url = str_replace(url(NULL, array('absolute' => TRUE)), '', $thisnode->field_linked_pages['und'][3]['url']);
                    $source = drupal_lookup_path('source', $url,'');
                    $nidParts = explode("/", $source);
                    $nidPart = (isset($nidParts[1]) ? $nidParts[1] : null);
                ?>

                <div class="grid-25 tablet-grid-50 mobile-grid-100 common-inner equal">
                    <div class="in-grid-item-wrapper common-inner">
                        <?php print views_embed_view('intro_block','home_four', $nidPart); ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
<?php endif; ?>
