<a href="#content" class="element-invisible element-focusable"><?php print t('Skip to content'); ?></a>

<?php if ($main_menu): ?>
    <a href="#navigation" class="element-invisible element-focusable" data-target=".nav-collapse" data-toggle="collapse"><?php print t('Skip to navigation'); ?></a>
<?php endif; ?>


<?php if ($page['pre_header']): ?>
    <div id="logged-in-panel" class="grid-container">
        <div class="grid-60 prefix-40 mobile-grid-100">
            <?php print render($page['pre_header']); ?>
        </div>
    </div>
<?php endif; ?>


<!-- /#admin-shortcuts -->
<?php if ($logo || $main_menu || ($page['header']) || ($page['search_box'])): ?>

<header role="banner">
    <div class="grid-container">


        <div class="grid-20 tablet-grid-20 mobile-grid-100 grid-parent">
            <?php if ($logo): ?>
                <div class="mobile-grid-50" id="header-logo">
                    <?php if ($is_front):?>
                        <h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"></a></h1>
                    <?php else:?>
                        <div><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"></a></div>
                    <?php endif;?>
                </div>
            <?php endif; ?>


            <div style="clear:both;" class="mobile-grid-50 right-align hide-on-desktop hide-on-tablet">

                <?php if ($secondary_nav  && ! $crappymenu): ?>
                    <div class="in-grid-item-wrapper" id="header-aux-links-mobile">
                        <?php print render($secondary_nav); ?>
                    </div>
                <?php endif; ?>

                <?php
                    if ( ! $crappymenu) {
                        $link = array(
                            '#theme' => 'link',
                            '#text' => 'Donate Now',
                            '#path' => theme_get_setting('donate_form'),
                            '#options' => array('attributes' => array('title' => 'Donate Now'), 'html' => false),
                            '#prefix' => '<div class="in-grid-item-wrapper" id="header-aux-donate-mobile"><div class="button-wrapper-green">',
                            '#suffix' => '</div></div>'
                        );

                        print render($link);
                    }
                ?>
            </div>
        </div>




        <div class="grid-60 prefix-20 grid-parent tablet-grid-70 tablet-prefix-10 mobile-grid-100 hide-on-mobile" id="header-aux">

            <?php if ($secondary_nav && ! $crappymenu): ?>
                <div class="grid-100 tablet-grid-100 mobile-grid-50 mobile-prefix-50 right-align mobile-breaker-10 hide-on-mobile" id="header-aux-links">
                    <div class="in-grid-item-wrapper user-menu">
                        <?php print render($secondary_nav); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($page['header'] && ! $crappymenu): ?>
                <?php print render($page['header']); ?>
            <?php endif; ?>


            <div class="grid-33 tablet-grid-33 mobile-grid-50 mobile-push-50 right-align mobile-breaker-10" id="header-aux-social">
                <div class="in-grid-item-wrapper">
                <!-- Add This -->
                </div>
            </div>


            <?php
                if ( ! $crappymenu) {
                    $link = array(
                        '#theme' => 'link',
                        '#text' => 'Donate NOW',
                        '#path' => theme_get_setting('donate_form'),
                        '#options' => array('attributes' => array('title' => 'DONATE'), 'html' => false),
                        '#prefix' => '<div class="grid-33 tablet-grid-33 mobile-grid-100 right-align mobile-breaker-10" id="header-aux-donate"><div class="in-grid-item-wrapper"><div class="button-wrapper-green"><span class="donate-icon"></span>',
                        '#suffix' => '</div></div></div>'
                    );

                    print render($link);
                }
            ?>

        </div>




        <?php if ($main_menu): ?>
        <div class="grid-100 tablet-grid-100 mobile-grid-100 navigation">
            <div class="in-grid-item-wrapper">
                <nav>
                    <?php if ($crappymenu): ?>
                        <ul class="donationmenu menu sf-menu">
                            <li class="first pretend <?php echo (intval($donatesection) == 1) ? 'active':'';?>">Donation details</li>
                            <li class="pretend <?php echo (intval($donatesection) == 2) ? 'active':'';?>">Personal details</li>
                            <li class="pretend <?php echo (intval($donatesection) == 3) ? 'active':'';?>">Payment details</li>
                        </ul>
                    <?php else: ?>

                        <?php if ( ! empty($page['navigation'])): ?>
                            <?php print render($page['navigation']); /** Superfish **/ ?>
                        <?php else: ?>
                            <?php print render($primary_nav); ?>
                        <?php endif; ?>

                    <?php endif; ?>
                </nav>
            </div>
        </div>
        <?php endif; ?>




    </div>
</header>
<?php endif; ?>








<div id="main" class="main" role="main">

<?php if ($breadcrumb): ?>
    <div class="grid-container" id="breadcrumbs">
        <div class="grid-100 tablet-grid-100 mobile-grid-100">
            <?php print $breadcrumb; ?>
        </div>
    </div>
<?php endif; ?>



<?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
<div class="grid-container">
    <div class="grid-100 tablet-grid-100 mobile-grid-100">
        <div class="in-grid-item-wrapper">
            <div class="tabs"> <?php print render($tabs); ?> </div>
            <div class="breaker-10 clearfix"></div>
        </div>
    </div>
</div>
<?php endif; ?>



<?php if ($messages): ?>
    <div id="console" class="clearfix grid-container">
        <?php print $messages; ?>
    </div>
<?php endif; ?>


<?php if ($page['help']): ?>
    <div id="help" class="clearfix">
        <?php print render($page['help']); ?>
    </div>
<?php endif; ?>


<?php if ($action_links): ?>
    <ul class="action-links">
        <?php print render($action_links); ?>
    </ul>
<?php endif; ?>