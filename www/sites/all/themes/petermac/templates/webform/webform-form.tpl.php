<?php if ($form['#node']->vuuid == 'b3db6acb-0891-4137-9934-3c644a3b3a4e') { ?>

<div class="webform-layout-box">
  <?php
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);

  // Print out the preview message if on the preview page.
  if (isset($form['preview_message'])) {
    print '<div class="messages warning">';
    print drupal_render($form['preview_message']);
    print '</div>';
  }

  ?>

  <?php print drupal_render($form['submitted']['about_you_title']); ?>
  <?php print drupal_render($form['submitted']['about_your_activity_title']); ?>
  <?php print drupal_render($form['submitted']['your_fundraising_title']); ?>
  <?php print drupal_render($form['submitted']['terms_and_conditions_title']); ?>
  <!-- Left Column -->
  <div class='grid-50 tablet-grid-50 mobile-grid-100 webform-content-left'>
    <?php print drupal_render($form['submitted']['title']); ?>
    <?php print drupal_render($form['submitted']['first_name']); ?>
    <?php print drupal_render($form['submitted']['last_name']); ?>
    <?php print drupal_render($form['submitted']['email_address']); ?>
    <?php print drupal_render($form['submitted']['your_date_of_birth']); ?>
    <?php print drupal_render($form['submitted']['date_of_birth_error_label']); ?>

    <?php print drupal_render($form['submitted']['activity_name']); ?>
    <?php print drupal_render($form['submitted']['start_date']); ?>
    <?php print drupal_render($form['submitted']['start_date_error_label']); ?>
    <?php print drupal_render($form['submitted']['duration']); ?>
    <?php print drupal_render($form['submitted']['fundraise_in_states_other_than_victoria']); ?>
    <?php print drupal_render($form['submitted']['if_yes_please_list_the_states_territories']); ?>

    <?php print drupal_render($form['submitted']['raffles_and_auctions']); ?>
    <?php print drupal_render($form['submitted']['how_will_the_proceeds_be_allocated_to_peter_mac']); ?>
    <?php print drupal_render($form['submitted']['describe_how_the_funds_will_be_raised']); ?>

    <?php print drupal_render($form['submitted']['do_you_agree_to_the_following_statements']); ?>
    <?php print drupal_render($form['submitted']['publicity_and_marketing']); ?>


  </div>

  <!-- Right Column -->
  <div class='grid-50 tablet-grid-50 mobile-grid-100 webform-content-right'>
    <?php print drupal_render($form['submitted']['street_address']); ?>
    <?php print drupal_render($form['submitted']['town_or_city']); ?>
    <?php print drupal_render($form['submitted']['state']); ?>
    <?php print drupal_render($form['submitted']['postcode']); ?>
    <?php print drupal_render($form['submitted']['phone']); ?>

    <?php print drupal_render($form['submitted']['describe_the_proposed_activity']); ?>
    <?php print drupal_render($form['submitted']['what_prompted_you_to_support_peter_mac']); ?>
    <?php print drupal_render($form['submitted']['how_did_you_hear_about_us']); ?>

    <?php print drupal_render($form['submitted']['income_and_expenses']); ?>
    <?php print drupal_render($form['submitted']['income_and_expenses_error_label']); ?>

    <?php print drupal_render($form['submitted']['public_liability']); ?>
    <?php print drupal_render($form['submitted']['please_provide_any_relevant_information_about_the_insurance_arrangements_for_your_event']); ?>
    <?php print drupal_render($form['submitted']['declaration']); ?>

    <!--Rest of the form-->
    <?php print drupal_render_children($form); ?>
  </div>
</div>

<?php } else { ?>

  <?php
    // Print out the progress bar at the top of the page
    print drupal_render($form['progressbar']);

    // Print out the preview message if on the preview page.
    if (isset($form['preview_message'])) {
      print '<div class="messages warning">';
      print drupal_render($form['preview_message']);
      print '</div>';
    }

    // Print out the main part of the form.
    // Feel free to break this up and move the pieces within the array.
    print drupal_render($form['submitted']);

    // Always print out the entire $form. This renders the remaining pieces of the
    // form that haven't yet been rendered above (buttons, hidden elements, etc).
    print drupal_render_children($form);

  ?>


<?php } ?>
