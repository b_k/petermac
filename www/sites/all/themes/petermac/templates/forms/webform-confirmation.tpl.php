<?php
//webform-confirmation.tpl.php
?>

<h1 property="dc:title" datatype=""><?php echo $node->title; ?></h1>

<?php if ($confirmation_message): ?>
	<?php print $confirmation_message ?>
<?php else: ?>
	<p><?php print t('Thank you, your submission has been received.'); ?></p>
<?php endif; ?>

<div class="links">
	<a href="<?php print url('node/'. $node->nid) ?>"><?php print t('Go back to the form') ?></a>
</div>