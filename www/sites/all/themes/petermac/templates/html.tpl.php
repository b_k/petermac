<!DOCTYPE html>
<!-- [if lt IE 7 ]><html class="no-js ie6" lang="en"> <![endif] -->
<!-- [if IE 7 ]><html class="no-js ie7" lang="en"> <![endif] -->
<!-- [if IE 8 ]><html class="no-js ie8" lang="en"> <![endif] -->
<!-- [if IE 9 ]><html class="no-js ie9" lang="en"> <![endif] -->
<!-- [if (gt IE 9)|!(IE)]><! --><html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="no-js"> <!-- <![endif] -->
<head>
  <meta charset="utf-8" />
  <?php print $head; ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php print $head_title; ?></title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
  <!-- IE Compatibility Metas -->
  <!-- [if lt IE 9]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <![endif] -->

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- [if lt IE 9]>
  <link rel="stylesheet" type="text/css" media="all" href="<?php print base_path() . drupal_get_path('theme', 'petermac');?>/styles/ie.css">
  <![endif] -->

  <!--[if (gte IE 6)&(lte IE 9)]>
  <script src="<?php print base_path() . drupal_get_path('theme', 'petermac');?>/scripts/vendor/selectivizr-1.0.2.min.js"></script>
  <![endif]-->

  <!-- Modernizr -->
  <script src="<?php print base_path() . drupal_get_path('theme', 'petermac');?>/scripts/vendor/modernizr-2.6.2.min.js"></script>

  <!-- [if lt IE 9]>
  <script src="<?php print base_path() . drupal_get_path('theme', 'petermac');?>/scripts/vendor/html5shiv.js"></script>
  <![endif] -->

  <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/8f8aa2d3-3122-42d0-895a-c00c5a60cb4a.css">

  <!-- AutoPilot for marketing automation-->
  <script type="text/javascript">(function(o){var b="https://api.autopilothq.com/anywhere/",t="6f0383e5e34840b28b67df4c2dd2755baa4070369a5a4158bfc65a68172c2d57",a=window.AutopilotAnywhere={_runQueue:[],run:function(){this._runQueue.push(arguments);}},c=encodeURIComponent,s="SCRIPT",d=document,l=d.getElementsByTagName(s)[0],p="t="+c(d.title||"")+"&u="+c(d.location.href||"")+"&r="+c(d.referrer||""),j="text/javascript",z,y;if(!window.Autopilot) window.Autopilot=a;if(o.app) p="devmode=true&"+p;z=function(src,asy){var e=d.createElement(s);e.src=src;e.type=j;e.async=asy;l.parentNode.insertBefore(e,l);};if(!o.noaa){z(b+"aa/"+t+'?'+p,false)};y=function(){z(b+t+'?'+p,true);};if(window.attachEvent){window.attachEvent("onload",y);}else{window.addEventListener("load",y,false);}})({});</script>
  <!-- AutoPilot for marketing automation /-->

  <?php print $styles; ?>

  <?php print $scripts; ?>
</head>

<body class="<?php print $classes; ?> <?php print $body_bg_type; ?> <?php print $body_bg_classes; ?> <?php print $content_order_classes; ?> <?php print $front_heading_classes; ?> <?php print $breadcrumb_classes; ?> <?php print $border_classes; ?> <?php print $corner_classes; ?>" <?php print $attributes;?> <?php if ($body_bg_classes): ?>style="background: url('<?php print file_create_url(theme_get_setting('body_bg_path')); ?>') repeat top left;" <?php endif; ?>>

  <div id="wrapper">
    <?php print $page_top; ?>
    <?php print $page; ?>

  </div>

  <?php print $page_bottom; ?>

</body>
</html>
