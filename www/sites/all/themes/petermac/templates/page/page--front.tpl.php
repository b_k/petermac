<?php include drupal_get_path('theme', 'petermac') . '/includes/page.header.inc'; ?>

<?php
  $thisnode = node_load(1);
?>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Peter Mac Homepage
URL of the webpage where the tag is expected to be placed: https://foundation.petermac.org/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 12/18/2013
-->

<script type="text/javascript">
  var axel = Math.random() + "";
  var a = axel * 10000000000000;
  document.write('<iframe src="https://4268656.fls.doubleclick.net/activityi;src=4268656;type=peter097;cat=peter426;ord=1;num=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>

<noscript>
  <iframe src="https://4268656.fls.doubleclick.net/activityi;src=4268656;type=peter097;cat=peter426;ord=1;num=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>


  <div class="grid-container">
    <?php if ($page['main_top']): ?>
      <?php print render($page['main_top']); ?>
    <?php endif; ?>
  </div>

  <div class="breaker-20 clearfix"></div>

  <div class="grid-container">
  	<div class="grid-100">
  	  <div class="in-grid-item-wrapper">
    		<div class="three-color-strip">
    		  <div class="left"></div>
    		  <div class="right"></div>
    		  <div class="center"></div>
    		</div>

        <div class="pad-20-wrapper common-inner">
          <?php if (($page['content']) || ($feed_icons)): ?>
            <?php print render($page['content']); ?>
          <?php endif; ?>
        </div>
  	  </div>
  	</div>
  </div>


  <?php include drupal_get_path('theme', 'petermac') . '/includes/linked.pages.inc'; ?>


<?php include drupal_get_path('theme', 'petermac') . '/includes/page.footer.inc'; ?>