<?php include drupal_get_path('theme', 'petermac') . '/includes/page.header.inc'; ?>

<div class="grid-container">
    <div class="grid-100 tablet-grid-100 mobile-grid-100">
        <div class="in-grid-item-wrapper">
            <div class="three-color-strip">
                <div class="left"></div>
                <div class="right"></div>
                <div class="center"></div>
            </div>
        </div>
    </div>
</div>

<div class="grid-container">
    <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
        <div class="grid-66 tablet-grid-100 mobile-grid-100">
            <div class="in-grid-item-wrapper common-inner pad-10-wrapper">
                <?php print drupal_render($page['content']); ?>
                <div class="breaker-20 clearfix"></div>
            </div>
        </div>

        <div class="grid-33 tablet-grid-33 mobile-grid-100 hide-on-tablet hide-on-mobile">
            <div class="in-grid-item-wrapper common-inner pad-10-wrapper mobile-pad-20-wrapper">

                <?php print render($page['sidebar_second']); ?>

                <div class="common-image">
                    <img src="<?php echo $base_path .drupal_get_path('theme', 'petermac');?>/images/placeholders/form-fields-0-0.jpg" alt="Full image description">
                </div>
                <div class="breaker-20 clearfix"></div>

                <?php if (arg(0) == 'user'): ?>
                    <div class="common-image">
                        <img src="<?php echo $base_path .drupal_get_path('theme', 'petermac');?>/images/placeholders/form-fields-0-1.jpg" alt="Full image description">
                    </div>
                    <div class="breaker-20 clearfix"></div>

                    <div class="common-image">
                        <img src="<?php echo $base_path .drupal_get_path('theme', 'petermac');?>/images/placeholders/form-fields-0-2.jpg" alt="Full image description">
                    </div>
                    <div class="breaker-30 clearfix"></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php include drupal_get_path('theme', 'petermac') . '/includes/page.footer.inc'; ?>