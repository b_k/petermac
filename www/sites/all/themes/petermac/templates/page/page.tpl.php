<?php include drupal_get_path('theme', 'petermac') . '/includes/page.header.inc'; ?>

  <?php if ($page['main_top']): ?>
    <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent"> <?php print render($page['main_top']); ?> </div>
  <?php endif; ?>

  <?php if (($page['content']) || ($feed_icons)): ?>
    <div id="content-body" class="row-fluid content-body">

      <?php if ( empty($page['content']['system_main']['nodes'])): ?>

        <?php /** For access denied template, search results, etc **/ ?>

        <div class="grid-container">
          <div class="grid-100 tablet-grid-100 mobile-grid-100">
            <div class="in-grid-item-wrapper">
              <div class="three-color-strip">
                <div class="left"></div>
                <div class="right"></div>
                <div class="center"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="grid-container">
            <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
                <div class="grid-100 tablet-grid-100 mobile-grid-100">
                    <div class="in-grid-item-wrapper common-inner pad-20-wrapper">
                        <div class="content">
                            <?php print render($page['content']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      <?php else: ?>

        <?php print render($page['content']); ?>

      <?php endif; ?>


      <?php print $feed_icons; ?>
    </div>
  <?php endif; ?>

  <?php include drupal_get_path('theme', 'petermac') . '/includes/linked.pages.inc'; ?>

<?php include drupal_get_path('theme', 'petermac') . '/includes/page.footer.inc'; ?>