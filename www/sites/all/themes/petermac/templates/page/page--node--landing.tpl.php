<?php include drupal_get_path('theme', 'petermac') . '/includes/page.header.inc'; ?>

  <div class="grid-container">
    <?php if ($page['main_top']): ?>
      <?php print render($page['main_top']); ?>
    <?php endif; ?>
  </div>

  <div class="breaker-20 clearfix"></div>

  <div class="grid-container">
  	<div class="grid-100 no-padding">
  	  <div class="in-grid-item-wrapper">
          <?php if (($page['content']) || ($feed_icons)): ?>
            <?php print render($page['content']); ?>
          <?php endif; ?>
  	  </div>
  	</div>
  </div>

<?php include drupal_get_path('theme', 'petermac') . '/includes/linked.pages.inc'; ?>

<?php include drupal_get_path('theme', 'petermac') . '/includes/page.footer.inc'; ?>
