<?php
//views-view-fields--home-three.tpl.php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */


print $fields['title']->wrapper_prefix;

if ($row->node_type == 'intro_block')
{
	print $fields['title']->content;
}
elseif (isset($fields['field_external_link_1']) && strlen(trim($fields['field_external_link_1']->raw) > 0 ))
{
	print '<h4><a href="'. strip_tags($fields['field_external_link_1']->content) . '">'. strip_tags($fields['title']->raw) .'</a></h4>';
}
elseif (isset($fields['field_external_link']) && strlen(trim($fields['field_external_link']->raw) > 0 ))
{
	$url = preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $fields['field_external_link']->content, $match);
	print '<h4><a href="'. strip_tags($match[2][0]) . '">'. strip_tags($fields['title']->raw) .'</a></h4>';
}
else
{
	print $fields['title']->content;
}

print $fields['title']->wrapper_suffix;



$ech = 0;

if ($row->node_type == 'intro_block') {
	$ech = 1;
	print $fields['field_intro_image']->content;

} elseif ($row->node_type == 'article') {
	$ech = 2 ;
	print $fields['field_image']->content;

} else {

	if ( ! empty($fields['field_menu_image']->content)) {
		$ech = 3;
		print $fields['field_menu_image']->content;
	} elseif ( ! empty($fields['field_mainimage']->content)) {
		$ech = 4;
		print $fields['field_mainimage']->content;
	}
}


if ($row->node_type == 'petermac_donate_simple') {
	print $fields['field_intro_image']->content;
}

print $fields['body']->wrapper_prefix;

// Standardize the output.
$contents = strip_tags($fields['body']->content, '<br><p>');

if ( ! stristr($contents, '<p>')) {
	$contents = '<p>' . $contents . '</p>';
}

print $contents;
print $fields['body']->wrapper_suffix;


if ($row->node_type == 'intro_block')
{
	print $fields['field_link']->content;
}
elseif ( ! empty($fields['field_external_link']->content))
{
	print $fields['field_external_link']->content;
}
else {
	if ( ! empty($fields['field_link_text']->content))
	{
		print $fields['field_link_text']->content;
	}
	else
	{
		print $fields['view_node']->content;
	}
}
?>




