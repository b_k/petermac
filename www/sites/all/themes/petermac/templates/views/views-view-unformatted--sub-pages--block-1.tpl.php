<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="breaker-20 clearfix"></div>

<?php
$i = 0;
foreach ($rows as $id => $row):
	$i++;
?>
  <div class="grid-container extra-outer-gutter">
	<div class="common-inner left-float">
      <?php print $row; ?>
	</div>
  </div>
<?php if ($i < count($rows)):?>
  <div class="grid-container extra-outer-gutter">
	<div class="grid-100 tablet-grid-100 mobile-grid-100 tablet-no-left-padding tablet-no-right-padding desktop-no-left-padding desktop-no-right-padding">
	  <div class="common-inner left-float-100 pad-10-wrapper-vertical">
		<div class="hr-style block"></div>
	  </div>
	</div>
  </div>
<?php endif;?>
<?php endforeach; ?>
