<?php
//views-view-fields--carousel.tpl.php
/*
* - $view: The view in use.
* - $fields: an array of $field objects. Each one contains:
*   - $field->content: The output of the field.
*   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
*   - $field->class: The safe class id to use.
*   - $field->handler: The Views field handler object controlling this field. Do not use
*     var_export to dump this object, as it can't handle the recursion.
*   - $field->inline: Whether or not the field should be inline.
*   - $field->inline_html: either div or span based on the above flag.
*   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
*   - $field->wrapper_suffix: The closing tag for the wrapper.
*   - $field->separator: an optional separator that may appear before a field.
*   - $field->label: The wrap label text to use.
*   - $field->label_html: The full HTML of the label to use including
*     configured element type.
* - $row: The raw result object from the query, with all data it fetched.
*/
?>

<div class="grid-65 tablet-grid-55 mobile-grid-100">
  <div class="in-grid-item-wrapper">
    <?php print $fields['field_carouselimage']->wrapper_prefix; ?>
    <?php print $fields['field_carouselimage']->label_html; ?>
    <?php print $fields['field_carouselimage']->content; ?>
    <?php print $fields['field_carouselimage']->wrapper_suffix; ?>
  </div>
</div>
<div class="grid-35 tablet-grid-45 mobile-grid-100 carousel-support">
  <div class="in-grid-item-wrapper">
    <div class="common-inner">
      <?php print $fields['title']->wrapper_prefix; ?>
      <?php print $fields['title']->label_html; ?>
      <?php print $fields['title']->content; ?>
      <?php print $fields['title']->wrapper_suffix; ?>
      <?php if ( ! empty($fields['field_body'])): ?>
        <?php print $fields['field_body']->wrapper_prefix; ?>
        <?php print $fields['field_body']->label_html; ?>
        <?php print $fields['field_body']->content; ?>
        <?php print $fields['field_body']->wrapper_suffix; ?>
      <?php endif; ?>
      <?php print $fields['field_linked_pages']->wrapper_prefix; ?>
      <?php print $fields['field_linked_pages']->label_html; ?>
      <?php print $fields['field_linked_pages']->content; ?>
      <?php print $fields['field_linked_pages']->wrapper_suffix; ?>
    </div>
  </div>
</div>

<?php foreach ($fields as $id => $field): ?>

  <?php if (!empty($field->separator)): ?>
  <?php print $field->separator; ?>
  <?php endif; ?>

<?php endforeach; ?>