<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="grid-container extra-outer-gutter">
	<div class="common-inner grid-100 tablet-grid-100 mobile-grid-100 left-float">
	<div class="hr-style top-20"></div>
	<?php if (!empty($title)): ?>
	  <h3><?php print $title; ?></h3>
	<?php endif; ?>

<?php
$i = 0;
foreach ($rows as $id => $row):
	$i++;
?>
  <div class="equal grid-33 tablet-grid-33 mobile-grid-100 <?php print ($i % 2) ? 'mobile-no-right-padding' : 'mobile-no-left-padding'; if ($classes_array[$id]) print  $classes_array[$id]; ?>">
	<div class="in-grid-item-wrapper common-inner pad-10-wrapper-vertical mobile-pad-10-wrapper">
    <?php print $row; ?>
    </div>
  </div>
<?php endforeach; ?>
	</div>
	<div class="breaker-100 clearfix"></div>
</div>