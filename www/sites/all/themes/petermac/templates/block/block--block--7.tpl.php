
<div class="breaker-20"></div>
<div class="grid-container">
  <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
    <div class="grid-100 tablet-grid-100 mobile-grid-100">
      <div class="in-grid-item-wrapper common-inner pad-20-wrapper clearfix">
        <?php if ($block->subject): ?>
          <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
        <?php endif;?>
        <?php print $content ?>
      </div>
    </div>
  </div>
</div>