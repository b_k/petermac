
<div class="breaker-20 clearfix"></div>

<div class="grid-100 tablet-grid-100 mobile-grid-100" id="news">
    <?php if (!empty($block->subject)) : ?>
        <h4><?php print $block->subject; ?></h4>
    <?php endif; ?>

    <?php print $content ?>
</div>