<div class="breaker-20"></div>
<div class="grid-container">
  <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
    <div class="grid-100 tablet-grid-100 mobile-grid-100">
      <div class="in-grid-item-wrapper common-inner pad-20-wrapper clearfix promoted-two">
        <?php print $content ?>
      </div>
    </div>
  </div>
</div>