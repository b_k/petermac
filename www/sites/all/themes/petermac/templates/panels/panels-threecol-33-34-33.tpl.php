<?php
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a three column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['middle']: Content in the middle column.
 *   - $content['right']: Content in the right column.
 */

?>

<div class="grid-container extra-outer-gutter">
    <div class="grid-33 tablet-grid-33 mobile-grid-100 mobile-breaker-10">
        <div class="in-grid-item-wrapper box-33-wrapper">
        	<?php print $content['left']; ?>
        </div>
	</div>
	<div class="grid-33 tablet-grid-33 mobile-grid-100 mobile-breaker-10">
		<div class="in-grid-item-wrapper box-33-wrapper">
            <?php print $content['middle']; ?>
        </div>
    </div>
	<div class="grid-33 tablet-grid-33 mobile-grid-100 mobile-breaker-10">
		<div class="in-grid-item-wrapper box-33-wrapper">
        	<?php print $content['right']; ?>
        </div>
    </div>
</div>
