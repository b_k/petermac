<?php

// Plugin definition
$plugin = array(
  'title' => t('Header with 65/35 split content'),
  'category' => t('Custom Layouts'),
  'icon' => 'header_65_35.png',
  'theme' => 'panels_header_65_35',
  'css' => 'header_65_35.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left'),
    'right' => t('Right')
  )
);