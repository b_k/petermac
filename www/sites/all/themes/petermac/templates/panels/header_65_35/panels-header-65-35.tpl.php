<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="grid-parent">
    <div class="grid-100 tablet-grid-100 mobile-grid-100 header-content">
      <div class="in-grid-item-wrapper">
        <?php print $content['header']; ?>
      </div>
    </div>
  </div>

  <div class="grid-parent">
    <div class="grid-65 tablet-grid-65 mobile-grid-100 mobile-breaker-10">
      <div class="in-grid-item-wrapper left-content">
        <?php print $content['left']; ?>
      </div>
    </div>
    <div class="grid-35 tablet-grid-35 mobile-grid-100 mobile-breaker-10 hide-for-mobile">
      <div class="common-inner padding-20">
        <div class="in-grid-item-wrapper right-content">
          <?php print $content['right']; ?>
        </div>
      </div>
    </div>
  </div>
</article>