
<div class="grid-container">
    <div class="grid-100 tablet-grid-100 mobile-grid-100">
        <div class="in-grid-item-wrapper">
            <div class="three-color-strip">
                <div class="left"></div>
                <div class="right"></div>
                <div class="center"></div>
            </div>
        </div>
    </div>
</div>

<div class="grid-container extra-outer-gutter">
    <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent donate-container-simple">

            <div class="grid-66 tablet-grid-100 mobile-grid-100 common-inner">
                <div class="in-grid-item-wrapper common-inner pad-20-wrapper mobile-pad-10-wrapper donate-body">


                    <h1><?php echo render($variables['title']); ?></h1>
                    <?php print render($content['body']); ?>
                    <div class="clearfix breaker-10"></div>
                    <?php print render($variables['elements']['form']); ?>
                    <div class="clearfix breaker-10"></div>
                    <?php echo render($variables['field_footer'][0]['value']); ?>
               </div>
            </div>

            <div class="grid-33 hide-on-mobile hide-on-tablet common-inner">
                <div class="in-grid-item-wrapper common-inner pad-10-wrapper-vertical mobile-pad-10-wrapper quote">
                    <?php echo render($variables['field_form_support'][0]['value']); ?>
                </div>
            </div>

    </div>
</div>