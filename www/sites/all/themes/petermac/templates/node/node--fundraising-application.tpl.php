<?php
// node--400.tpl.php
// Node ID base template used due to main webform template already being overwritten
?>
<?php if (!empty($content['field_mainimage'])):?>
<div class="grid-container">
	<div class="grid-100 tablet-grid-100 mobile-grid-100">
		<div class="common-image relative">
			<?php echo render($content['field_mainimage']); ?>
			<div class="on-image-intro">
				<!-- Add Title placement over the main header image -->
				<?php if (!$page): ?>
					<?php print render($title_prefix); ?>
					<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
					<?php print render($title_suffix); ?>
				<?php else: ?>
					<?php print render($title_prefix); ?>
					<h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
					<?php print render($title_suffix); ?>
				<?php endif; ?>
				<!-- Add Intro copy for placement over the main header image -->
				<div class="intro-copy">
					<?php print render($content['field_intro_copy']); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<div class="grid-container">
	<div class="grid-100 tablet-grid-100 mobile-grid-100">
		<div class="in-grid-item-wrapper common-inner pad-20-wrapper mobile-pad-10-wrapper clearfix">
			<div class=" restricted-width-75">
			<?php
				// We hide the comments and links now so that we can render them later.
				hide($content['comments']);
				hide($content['links']);
				print render($content['body']);
			?>
			</div>
			<?php
				webform_node_view($node,'full');
				print theme_webform_view($node->content);
			?>
		</div>
	</div>
</div>
