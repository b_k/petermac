<?php

// Including this will publish the Drupal contact form into the page body.
require_once drupal_get_path('module', 'contact') .'/contact.pages.inc';
?>


<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div class="grid-container">
        <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
            <div class="grid-100 tablet-grid-100 mobile-grid-100">
                <div class="in-grid-item-wrapper">
                    <div class="common-image">
                        <?php print render($content['field_mainimage']); ?>
                    </div>
                    <div class="three-color-strip">
                        <div class="left"></div>
                        <div class="right"></div>
                        <div class="center"></div>
                    </div>
                </div>
            </div>
            <div class="grid-100 tablet-grid-100 mobile-grid-100 grid-parent">
                <div class="grid-100 tablet-grid-100 mobile-grid-100">
                    <div class="in-grid-item-wrapper common-inner pad-20-wrapper">

                        <h2><?php print t('Contact us');?></h2>

			  			<?php
                        $form = drupal_get_form('contact_site_form');
			  			print drupal_render($form);
			  			?>

                        <div class="clearfix breaker-10"></div>

			  		</div>
			  	</div>
  			</div>
        </div>
	</div>



    <div class="grid-container">
        <div class="grid-100 tablet-grid-100 mobile-grid-100">
            <div class="common-inner left-float" style="width:100%">
                <div class="grid-50 tablet-grid-50 mobile-grid-100 mobile-breaker-10 equal">

                    <div class="in-grid-item-wrapper box-50-wrapper pad-10-wrapper">

                    <?php print render($title_prefix); ?>
                        <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                    <?php print render($title_suffix); ?>

                    <?php if ($display_submitted): ?>
                        <div class="submitted"> <?php print $submitted; ?> </div>
                    <?php endif; ?>


                    <div class="content"<?php print $content_attributes; ?>>
                    <?php
                      // We hide the comments and links now so that we can render them later.
                      hide($content['comments']);
                      hide($content['links']);

                      print render($content['body']);
                    ?>
                    </div>

                    <?php print render($content['links']); ?> <?php print render($content['comments']); ?>

                    </div>
                </div>

                <div class="grid-50 tablet-grid-50 mobile-grid-100 mobile-breaker-10 equal">
                    <div class="in-grid-item-wrapper box-50-wrapper pad-10-wrapper">
                        <iframe width="100%" height="450" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12610.32410475!2d144.9568963!3d-37.7998575!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa8de9acc71f5f760!2sPeter+MacCallum+Cancer+Foundation!5e0!3m2!1sen!2sau!4v1469169312646" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

</article>