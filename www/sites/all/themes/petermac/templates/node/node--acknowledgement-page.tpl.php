<?php if (!empty($content['field_mainimage'])):?>
<div class="grid-container">
	<div class="grid-100 tablet-grid-100 mobile-grid-100">
		<div class="common-image">
			<?php echo render($content['field_mainimage']); ?>
		</div>
	</div>
</div>
<?php endif; ?>
<div class="grid-container">
	<div class="grid-100 tablet-grid-100 mobile-grid-100">
		<div class="in-grid-item-wrapper">
			<div class="three-color-strip">
				<div class="left"></div>
				<div class="right"></div>
				<div class="center"></div>
			</div>
		</div>
	</div>
</div>
<div class="grid-container extra-outer-gutter">
	<div class="grid-100 tablet-grid-100 mobile-grid-100 common-inner grid-parent">

		<div class="grid-66 tablet-grid-66 mobile-grid-100">
			<div class="in-grid-item-wrapper common-inner pad-20-wrapper mobile-pad-10-wrapper">
               <?php if (!$page): ?>
                 <?php print render($title_prefix); ?>
                 <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
                 <?php print render($title_suffix); ?>
               <?php else: ?>
                 <?php print render($title_prefix); ?>
                 <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                 <?php print render($title_suffix); ?>
               <?php endif; ?>
               <?php
                  // We hide the comments and links now so that we can render them later.
                  hide($content['comments']);
                  hide($content['links']);
                  print render($content['body']);

        			    webform_node_view($node,'full');

                  if ( ! empty($node->content)) {
          			    print theme_webform_view($node->content);
                  }
			         ?>
            </div>
        </div>
        <div class="grid-33 tablet-grid-33 mobile-grid-100 hide-on-mobile hide-on-tablet">
            <div class="in-grid-item-wrapper common-inner pad-10-wrapper-vertical mobile-pad-10-wrapper">
                <?php echo render($content['field_right_sidebar']); ?>
            </div>
        </div>
    </div>
</div>

