<?php

// Including this will publish the Drupal contact form into the page body.
require_once drupal_get_path('module', 'contact') .'/contact.pages.inc';
?>


<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="grid-parent">
    <div class="grid-100 tablet-grid-100 mobile-grid-100 header-content">
        <div class="in-grid-item-wrapper">
            <div class="common-image">
                <?php print render($content['field_mainimage']); ?>
                <div class="intro-copy">
                  <?php print render($content['field_intro_copy']); ?>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="grid-parent">
    <div class="grid-65 tablet-grid-65 mobile-grid-100 mobile-breaker-10 equal">
        <div class="common-inner padding-20">
          <div class="in-grid-item-wrapper">
            <?php
              $webformID = $content['field_webform_id'][0]['#markup'];
              print render($content['field_left_column_content']);
              $block = module_invoke('webform', 'block_view', 'client-block-' . $webformID);
              print render($block['content']);
            ?>
          </div>
        </div>
    </div>
    <div class="grid-35 tablet-grid-35 mobile-grid-100 mobile-breaker-10 equal hide-for-mobile">
        <div class="common-inner padding-20">
          <div class="in-grid-item-wrapper">
              <?php print render($content['field_supporter_image']); ?>
              <?php print render($content['field_supporter_copy']); ?>
              <?php print render($content['field_supporter_name']); ?>
          </div>
        </div>
    </div>
  </div>


</article>
