var meanmenuWidth = 767;
(function($) {

    Drupal.behaviors.petermac_override = {
        attach: function(context, settings) {

            if ($('.fundraising-application-form input.webform-calendar:visible').length > 0) {
                $('input.webform-calendar')
                    .datepicker('option', 'changeMonth', true)
                    .datepicker('option', 'changeYear', true);
            }

            $('nav ul, #secondary-menu ul')
                .children('li')
                .filter(':has(.active)')
                .addClass('active')
                .end()
                .filter(':has(ul .active)')
                .removeClass('active')
                .end()
                .end()
                .find('ul a')
                .removeAttr('data-toggle')
                .removeAttr('data-target');

            $('a').each(function() {
                if (window.location.hostname && window.location.hostname !== this.hostname && this.href.indexOf('http') != -1) {
                    $(this).click(function() {
                        $(this).attr('target', '_blank');
                    });
                }
            });

            $('header nav').meanmenu({
                meanScreenWidth: meanmenuWidth,
                meanNavPush: 10,
                meanRemoveAttrs: true
            });

            $(".password-strength-text:contains('Weak')").css("color", "#c11f2e");
            $(".password-strength-text:contains('Fair')").css("color", "#ee8e1d");
            $(".password-strength-text:contains('Good')").css("color", "#5EB657");


            //  Fuild layout, centering the items
            if ($('.carousel-container ul').length > 0) {
                $('.carousel-container ul').carouFredSel({
                    width: '100%',
                    scroll: 1,
                    items: 1,
                    responsive: true,
                    pagination: '.carousel-pagination',

                    auto: {
                        timeoutDuration: 8000
                    }

                });
            }

            function totaliseFundraising() {
                this.init = function() {

                    if ($("form[id*='webform-client-form-126']").length) {
                        $("form[id*='webform-client-form-126']").on("input propertychange", "input[id*='income']", function() {
                            var total = 0;
                            $("input[id*='income']").each(function(i, el) {
                                if ($(this).attr('id').indexOf('total') == -1) {
                                    total += (isNaN(parseFloat(el.value, 10))) ? 0 : parseFloat(el.value, 10) ;
                                }
                            });
                            $("input[id*='income-total']").val(total);
                            $("input[id*='estimated-proceeds']").val($("input[id*='income-total']").val() - $("input[id*='total-expenses']").val());
                        });
                        $("form[id*='webform-client-form-126']").on("input propertychange", "input[id*='expenses']", function() {
                            var total = 0;
                            $("input[id*='expenses']").each(function(i, el) {
                                if ($(this).attr('id').indexOf('total') == -1) {
                                    total += (isNaN(parseFloat(el.value, 10))) ? 0 : parseFloat(el.value, 10) ;
                                }
                            });
                            $("input[id*='total-expenses']").val(total);
                            $("input[id*='estimated-proceeds']").val($("input[id*='income-total']").val() - $("input[id*='total-expenses']").val());
                        });
                    }

                }
                this.init();
            }

            function donationEnvelopes() {
                this.init = function() {
                    if ($("form[id*='webform-client-form-115']").length) {
                        $("input[id*='are-you-the-next-of-kin']").change(function() {
                            if ($(this).attr("checked")) {
                                $("input[id*='name-of-the-next-of-kin']").val($("input[id*='your-contact-details-your-first-name']").val() + ' ' + $("input[id*='your-contact-details-your-last-name']").val());
                                $("input[id*='next-of-kin-their-street-address']").val($("input[id*='your-postal-address-street-address']").val());
                                $("input[id*='next-of-kin-their-suburb']").val($("input[id*='your-postal-address-suburb']").val());
                                var state = $("select[id*='your-postal-address-state'] option:selected").val();
                                $("select[id*='next-of-kin-their-state'] option[value=" + state + "]").attr('selected', 'selected');
                                //$("input[id*='next-of-kin-their-state']").val($("input[id*='your-postal-address-state-select']").val());
                                $("input[id*='next-of-kin-their-postcode']").val($("input[id*='your-postal-address-postcode']").val());
                                $("input[id*='next-of-kin-their-email-address']").val($("input[id*='your-contact-details-your-email-address']").val());
                            }
                        });
                    }
                }
                this.init();
            }

            function tributeInfo() {
                this.init = function() {
                    if ($("form[id*='webform-client-form-166--2']").length) {
                        $("input[id*='mail-a-letter-on-my-behalf']").change(function() {
                            if ($(this).attr("checked")) {
                                $("#webform-component-tribute-info").slideDown();
                            }
                            else $("#webform-component-tribute-info").slideUp();
                        });
                        $("#webform-component-tribute-info").slideUp();
                    }
                    if ($("form[id*='webform-client-form-224--2']").length) {
                        $("input[id*='mail-a-letter-on-my-behalf']").change(function() {
                            if ($(this).attr("checked")) {
                                $("#webform-component-tribute-info").slideDown();
                            }
                            else $("#webform-component-tribute-info").slideUp();
                        });
                        $("#webform-component-tribute-info").slideUp();
                    }
                    if ($("form[id*='webform-client-form-225--2']").length) {
                        $("input[id*='mail-a-letter-on-my-behalf']").change(function() {
                            if ($(this).attr("checked")) {
                                $("#webform-component-tribute-info").slideDown();
                            }
                            else $("#webform-component-tribute-info").slideUp();
                        });
                        $("#webform-component-tribute-info").slideUp();
                    }

                }
                this.init();
            }

            function celebrationEnvelopes() {
                this.init = function() {
                    if ($("form[id*='webform-client-form-116--2']").length) {
                        $("input[id*='are-you-the-person-being-celebrated']").change(function() {
                            if ($(this).attr("checked")) {
                                $("input[id*='name-of-the-person-the-event-celebrates']").val($("input[id*='your-contact-details-your-first-name']").val() + ' ' + $("input[id*='your-contact-details-your-last-name']").val());
                                $("input[id*='person-being-celebrated-their-street-address']").val($("input[id*='your-postal-address-street-address']").val());
                                $("input[id*='person-being-celebrated-their-suburb']").val($("input[id*='your-postal-address-suburb']").val());
                                var state = $("select[id*='your-postal-address-state'] option:selected").val();
                                $("select[id*='person-being-celebrated-their-state'] option[value=" + state + "]").attr('selected', 'selected');
                                $("input[id*='person-being-celebrated-their-postcode']").val($("input[id*='your-postal-address-postcode']").val());
                                $("input[id*='person-being-celebrated-their-email-address']").val($("input[id*='your-contact-details-your-email-address']").val());
                            }
                        });
                    }
                }
                this.init();
            }

            function panelEqual() {
                this.init = function() {
                    $('.grid-container').each(function() {

                        var $sameHeightChildren = $(this).find('.equal');
                        var maxHeight = 0;

                        $sameHeightChildren.each(function() {
                            $(this).css("height", "");
                            maxHeight = Math.max(maxHeight, $(this).height());
                        });

                        $sameHeightChildren.css({
                            height: maxHeight + 'px'
                        });

                    });
                }
                this.init();
            }

            //main nav dropdowns

            function dropdown() {
                $('header .dropdown').hover(
                    function() {
                        menuThis = this;
                        $(menuThis).addClass('hover');
                        setTimeout(function() {
                            if ($(menuThis).hasClass('hover')) {
                                $(menuThis).children('.nav-sub').slideDown('fast');
                            }
                        }, 150)
                    },
                    function() {
                        menuThis = this;
                        $(menuThis).removeClass('hover');
                        $(menuThis).children('.nav-sub').slideUp('fast');
                    }
                )
                if (isiOS) {
                    $('.dropdown').click(function() {
                        $(this).unbind('click');
                        return false;
                    })
                    $('.nav-item').mouseleave(function() {
                        $(this).bind('click', function() {
                            $(this).unbind('click');
                            return false;
                        });
                    })
                }
            }

            function loginOpenClose() {
                $('#logged-in-panel').hide();
                $("a[href*='user/login']").click(function(e) {
                    if (window.location.href.indexOf("user") == -1) {
                        e.preventDefault();
                        $("#logged-in-panel").slideToggle(150);
                    }
                });
                $('.login .close-panel').click(function(e) {
                    e.preventDefault();
                    $("#logged-in-panel").slideUp(150);
                });
            }


            // Set up theme specific responsive behaviors
            function responsive_behaviors() {
                var width = $(window).width();

                if (width < 751) {
                    $('nav li li.expanded').removeClass('dropdown-submenu');
                    $('nav ul ul ul').removeClass('dropdown-menu');
                } else {
                    $('nav li li.expanded').addClass('dropdown-submenu');
                    $('nav ul ul ul').addClass('dropdown-menu');
                }
                panelEqual();
                if ((width >= 751) && (width < 963)) {
                    $('.two-sidebars')
                        .find('.site-sidebar-first')
                        .removeClass('grid-25')
                        .addClass('grid-33')
                        .end()
                        .find('.site-sidebar-second')
                        .removeClass('grid-25')
                        .addClass('grid-100')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-50')
                        .addClass('grid-66')
                        .end()
                        .find('.region-sidebar-second .block')
                        .addClass('grid-33')
                        .end();
                    $('.sidebar-first')
                        .find('.site-sidebar-first')
                        .removeClass('grid-25')
                        .addClass('grid-33')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-75')
                        .addClass('grid-66')
                        .end();
                    $('.sidebar-second')
                        .find('.site-sidebar-second')
                        .removeClass('grid-25')
                        .addClass('grid-100')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-75')
                        .addClass('grid-100')
                        .end()
                        .find('.region-sidebar-second .block')
                        .addClass('grid-33')
                        .end();
                } else {
                    $('.two-sidebars')
                        .find('.site-sidebar-first')
                        .removeClass('grid-33')
                        .addClass('grid-25')
                        .end()
                        .find('.site-sidebar-second')
                        .removeClass('grid-100')
                        .addClass('grid-25')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-66')
                        .addClass('grid-50')
                        .end()
                        .find('.region-sidebar-second .block')
                        .removeClass('grid-33')
                        .end();
                    $('.sidebar-first')
                        .find('.site-sidebar-first')
                        .removeClass('grid-33')
                        .addClass('grid-25')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-66')
                        .addClass('grid-75')
                        .end();
                    $('.sidebar-second')
                        .find('.site-sidebar-second')
                        .removeClass('grid-100')
                        .addClass('grid-25')
                        .end()
                        .find('.mc-content')
                        .removeClass('grid-100')
                        .addClass('grid-75')
                        .end()
                        .find('.region-sidebar-second .block')
                        .removeClass('grid-33')
                        .end();
                }
            }
            // Update CSS classes based on window load and resize
            $(window)
                .load(responsive_behaviors)
                .resize(responsive_behaviors);
            $(document)
                .ready(function() {
                    $('body').addClass('jsactive');
                    panelEqual();
                    totaliseFundraising();
                    donationEnvelopes();
                    celebrationEnvelopes();
                    tributeInfo();
                    if (ie && ie < 8) {
                        $('hr').replaceWith('<div class="hr"></div>')
                    }
                    document.getElementsByTagName("HTML")[0].style.margin = "0";

                    $(".expandable").click(function() {
                        if ($(window).width() < 768) {
                            $($(this).nextAll(".expandable-content")[0]).slideToggle("slow", function() {
                                $(this).prevAll(".expandable").toggleClass('expanded');
                            });
                        }
                    });
                });

            // add some ux to calc fields
            $('.calc-field').click(function() {
                if ($(this).val() == '0' || $(this).val() == '0.00') {
                    $(this).val('');
                }
            }).blur(function() {
                if ($(this).val() == '') {
                    $(this).val('0.00');
                }
            });

            jQuery(document).ready(function($) {

                // move the help text from below the input field to directly below the field label

                $('.form-item .description').each(function() { // description is the help text
                    var desc = $(this);
                    var label = desc.siblings('label:first');
                    if (label.length) {
                        desc.insertAfter(label);
                    }
                })
                // the help text is tangled up in the text format stuff on a filtered text field
                $('.text-format-wrapper .description').each(function() {
                    var desc = $(this);
                    var label = desc.siblings('.form-item').find('label:first');
                    if (label.length) {
                        desc.insertAfter(label);
                    }
                })
            });

            jQuery(document).ready(function($) {

                //
                // fixes to fundraising form validation
                //

                jQuery.validator.addMethod("ausPostcode", function(value, element) {
                    return this.optional(element) || /^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/.test(value);
                }, 'Please enter a valid postcode.');

                jQuery.validator.addMethod("ausPhone", function(value, element) {
                    return this.optional(element) || /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/.test(value);
                }, 'Please enter a valid phone number.');

                if ($('.fra-postcode').is(':visible')) {
                    $('.fra-postcode').rules('add', {
                        ausPostcode: true
                    });
                }
                if ($('.fra-phone').is(':visible')) {
                    $('.fra-phone').rules('add', {
                        ausPhone: true
                    });
                }

                // custom hacky validation
                $('.fundraising-application-form .form-submit').click(function(e) {
                    if ($('#formula-component-total_income:visible') && parseFloat($('#formula-component-total_income').html()) == 0) {
                        e.preventDefault();
                        $('#no_income_error').show();
                        $('.webform-component--income-and-expenses--income')[0].scrollIntoView();
                    } else {
                        $('#no_income_error').hide();
                    }
                });

                $('.fra-howheard.select-or-other-other').attr('placeholder', 'Tell us more');

            });

        }
    }

})(jQuery);

var isiPhone = (navigator.userAgent.indexOf("iPhone") != -1) ? true : false;
var isiPod = (navigator.userAgent.indexOf("iPod") != -1) ? true : false;
var isiPad = (navigator.userAgent.indexOf("iPad") != -1) ? true : false;
var isiOS = isiPhone || isiPad || isiPod;

var ie = (function() {
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
    return v > 4 ? v : undef;
}());
